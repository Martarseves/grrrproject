const { getConnection } = require("../db");
const { generateError } = require("../helpers");


async function listWishlist(req, res, next) {
  let connection;
  try {
    const { userId } = req.params;
    connection = await getConnection();

    const [result] = await connection.query(
      `
    SELECT pr.id AS id, pr.name, shops_id, s.name AS shopName, category, price, available, 
<<<<<<< HEAD
    type, photo, avg(rating) AS avgRating, COUNT(rating) AS votes 
    from wishlists w 
=======
      type, photo, avg(rating) AS avgRating, COUNT(rating) AS votes 
      from wishlists w 
>>>>>>> final commit
    LEFT JOIN products pr ON w.products_id = pr.id 
    LEFT JOIN ratings r ON pr.id = r.products_id 
    LEFT JOIN shops s ON shops_id = s.id
    WHERE w.users_id=? group by pr.id
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [userId]
    );

    const [user] = await connection.query(
      `
    SELECT first_name FROM users WHERE id=?
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [userId]
    );

    res.send({
      status: "ok",
      result,
      user,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}


async function addToWishlist(req, res, next) {
  let connection;
  try {
    const { userId } = req.auth;
    const { productId } = req.params;
    connection = await getConnection();
    const [result] = await connection.query(
      `
SELECT id FROM wishlists WHERE users_id=? AND products_id=?
`,
      [userId, productId]
    );

    if (result.length) {
      throw generateError(`This product is already in your wishlist`, 409);
    }

    await connection.query(
      `
<<<<<<< HEAD
INSERT INTO wishlists (users_id, products_id)
VALUES(?, ?)
`,
      [userId, productId]
    );

=======
    INSERT INTO wishlists (users_id, products_id)
    VALUES(?, ?)
      `,
      [userId, productId]
    );
>>>>>>> final commit
    res.send({
      status: "ok",
      message: "Product added to wishlist.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}


async function deleteFromWishlist(req, res, next) {
  let connection;
  try {
    const { userId } = req.auth;
    const { productId } = req.params;
    connection = await getConnection();

    const [result] = await connection.query(
      `
<<<<<<< HEAD
DELETE FROM wishlists WHERE users_id=? AND products_id=?
`,
=======
    DELETE FROM wishlists WHERE users_id=? AND products_id=?
      `,
>>>>>>> final commit
      [userId, productId]
    );

    if (result.affectedRows === 0) {
      throw generateError("This product was not in your wishlist.", 400);
    }

    res.send({
      status: "ok",
      message: "Product removed from wishlist.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

module.exports = {
  listWishlist,
  addToWishlist,
  deleteFromWishlist,
};
