require("dotenv").config();

const { getConnection } = require("../db");
const { generateError } = require("../helpers");

const { addressSchema } = require("./validations");

async function listAddress(req, res, next) {
  let connection;
  try {
    const { userId } = req.auth;

    connection = await getConnection();

    const [addresses] = await connection.query(
      `
    SELECT id as addressId, alias, name, street, council FROM addresses WHERE users_id=?
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [userId]
    );

    res.send({
      status: "ok",
      addresses,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function addAddress(req, res, next) {
  let connection;
  try {
    const { address } = req.body;
    await addressSchema.validateAsync(address);
    const { userId } = req.auth;
    const {
      alias,
      name,
     street,
     council
    } = address;

    connection = await getConnection();

    await connection.query(
      `
<<<<<<< HEAD
  INSERT INTO addresses (users_id, alias, name, street, council)
  VALUES (?, ?, ?, ?, ?)
  `,
=======
    INSERT INTO addresses (users_id, alias, name, street, council)
    VALUES (?, ?, ?, ?, ?)
      `,
>>>>>>> final commit
      [userId, alias, name, street, council]);

    res.send({
      status: "ok",
      message: "Address added successfully.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function editAddress(req, res, next) {
  let connection;
  try {
    const { address } = req.body;
    await addressSchema.validateAsync(address);
    const { userId, role } = req.auth;
    const { addressId } = req.params;

    connection = await getConnection();

    const [result] = await connection.query(
      `
<<<<<<< HEAD
SELECT users_id FROM addresses WHERE id=?
`,
=======
    SELECT users_id FROM addresses WHERE id=?
      `,
>>>>>>> final commit
      [addressId]
    );

    const [user] = result;

    if (userId !== user.users_id && role !== "admin") {
<<<<<<< HEAD
      throw generateError(`You can not change this address.`, 401);
=======
      throw generateError(`You cannot change this address.`, 401);
>>>>>>> final commit
    }

    const {
      alias,
      name,
     street,
     council
    } = address;

    await connection.query(
      `
<<<<<<< HEAD
  UPDATE addresses SET alias =?, name=?, street=?, council=? WHERE id=?
  `,
=======
    UPDATE addresses SET alias =?, name=?, street=?, council=? WHERE id=?
      `,
>>>>>>> final commit
      [
        alias,
        name,
        street,
        council,
        addressId,
      ]
    );

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      message: "Address updated successfully.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function deleteAddress(req, res, next) {
  let connection;
  try {
    const { userId, role } = req.auth;
    const { addressId } = req.params;

    connection = await getConnection();
    const [result] = await connection.query(
      `
<<<<<<< HEAD
SELECT users_id FROM addresses WHERE id=?
`,
=======
    SELECT users_id FROM addresses WHERE id=?
      `,
>>>>>>> final commit
      [addressId]
    );

    const [user] = result;

    if (userId !== user.users_id && role !== "admin") {
<<<<<<< HEAD
      throw generateError(`You can not delete this address.`, 401);
=======
      throw generateError(`You cannot delete this address.`, 401);
>>>>>>> final commit
    }

    await connection.query(
      `
<<<<<<< HEAD
  DELETE FROM addresses WHERE id=?
  `,
=======
    DELETE FROM addresses WHERE id=?
      `,
>>>>>>> final commit
      [addressId]
    );

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      message: "Address deleted successfully.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

module.exports = {
  listAddress,
  addAddress,
  editAddress,
  deleteAddress,
};
