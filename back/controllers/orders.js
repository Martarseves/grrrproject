const { getConnection } = require("../db");
const { generateError, sendEmail } = require("../helpers");

//SHOW CART
async function showCurrentCart(req, res, next) {
  let connection;
  try {
    const { userId } = req.auth;
    connection = await getConnection();

    const [result] = await connection.query(
      `
      SELECT pr.id AS id, pr.name, shops_id, s.name AS shopName, category, pr.price, available, 
<<<<<<< HEAD
    type, photo, avg(rating) AS avgRating, COUNT(rating) AS votes
      FROM orders_products op 
      LEFT JOIN orders o ON op.orders_id=o.id 
      LEFT JOIN products pr ON op.products_id=pr.id 
    LEFT JOIN ratings r ON pr.id = r.products_id 
          LEFT JOIN shops s ON shops_id = s.id
=======
        type, photo, avg(rating) AS avgRating, COUNT(rating) AS votes
      FROM orders_products op 
      LEFT JOIN orders o ON op.orders_id=o.id 
      LEFT JOIN products pr ON op.products_id=pr.id 
      LEFT JOIN ratings r ON pr.id = r.products_id 
      LEFT JOIN shops s ON shops_id = s.id
>>>>>>> final commit
      WHERE o.users_id=? AND finished=0 group by pr.id;
      `,
      [userId]
    );

    const [addresses] = await connection.query(
      `
      SELECT id, alias FROM addresses WHERE users_id=?;
      `,
      [userId]
    );

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      result,
      addresses,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

//SHOW FINISHED ORDERS
async function showFinishedOrders(req, res, next) {
  let connection;
  try {
    const { userId } = req.auth;
    connection = await getConnection();

    const [result] = await connection.query(
      `
    SELECT o.id, sell_date, addresses_id AS address, alias, SUM(op.price) as price FROM orders o 
    LEFT JOIN orders_products op ON o.id = op.orders_id 
    LEFT JOIN addresses a ON o.addresses_id = a.id 
    WHERE o.users_id=? AND finished = 1 group by o.id 
    ORDER BY o.mod_date DESC
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [userId]
    );

    for (const order of result) {
      const [products] = await connection.query(
        `
    SELECT  pr.id AS productId, pr.name, op.price as price, photo, 
    quantity, rating from  orders_products op 
    LEFT JOIN  products pr ON op.products_id = pr.id 
    LEFT JOIN orders o ON op.orders_id = o.id
    LEFT JOIN ratings r ON op.products_id = r.products_id AND o.users_id = r.users_id
<<<<<<< HEAD
     WHERE orders_id=?;
   
    `,
        [order.id]
      );
=======
    WHERE orders_id=?;
       `,
      [order.id]
    );
>>>>>>> final commit

      order.products = products;
    }
    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      result,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

<<<<<<< HEAD
=======


>>>>>>> final commit
async function addToCart(req, res, next) {
  let connection;
  try {
    const { userId } = req.auth;
    const { productId } = req.params;
    let { units } = req.query;
    if (!units) {
      units = 1;
    }
    connection = await getConnection();

    const [product] = await connection.query(
    `
    SELECT stock FROM products WHERE id=?
    `,
      [productId]
    );

    if (!product.length) {
      throw generateError(`Product not found`, 404);
    }

    const { stock } = product[0];

    if (stock < units) {
      throw generateError(`There's not enough units`, 409);
    }

    let [result] = await connection.query(
    `
    SELECT id FROM orders WHERE users_id=? AND finished = 0
    `,
      [userId]
    );

    let [order] = result;
    let orderId;
    if (order) {
      orderId = order.id;
    } else {
      result = await connection.query(
        `
      INSERT INTO orders (users_id)
      VALUES(?)
<<<<<<< HEAD
      `,
=======
        `,
>>>>>>> final commit
        [userId]
      );

      orderId = result[0].insertId;
    }

    const [productInCart] = await connection.query(
      `
     SELECT quantity FROM orders_products WHERE orders_id=? AND products_id=?
      `,
      [orderId, productId]
    );

    if (productInCart.length) {
      const { quantity } = productInCart[0];
      if (stock < Number(units) + quantity) {
        throw generateError(`There's not enough units left.`, 409);
      }
      connection.query(
        `
        UPDATE orders_products SET quantity=? WHERE orders_id=? AND products_id=?
<<<<<<< HEAD
      `,
        [quantity + +units, orderId, productId]
=======
        `,
        [quantity, units, orderId, productId]
>>>>>>> final commit
      );
    } else {
      await connection.query(
        `
<<<<<<< HEAD
INSERT INTO orders_products (orders_id, products_id, quantity)
VALUES(?, ?, ?)
`,
=======
        INSERT INTO orders_products (orders_id, products_id, quantity)
        VALUES(?, ?, ?)
        `,
>>>>>>> final commit
        [orderId, productId, units]
      );
    }

    res.send({
<<<<<<< HEAD
      status: "ok",
      message: "Product added to cart.",
=======
      status: "Ok",
      message: "Product successfully added to cart.",
>>>>>>> final commit
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

<<<<<<< HEAD
//DELETE PRODUCT FROM ORDER
=======

>>>>>>> final commit
async function deleteFromCart(req, res, next) {
  let connection;
  try {
    const { userId } = req.auth;
    const { productId } = req.params;
    connection = await getConnection();
    const [orderResult] = await connection.query(
      `
    SELECT id FROM orders WHERE users_id=? AND finished=0
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [userId]
    );

    const [order] = orderResult;

    if (!order) {
      throw generateError(`You do not have a current order.`, 404);
    }
    const [result] = await connection.query(
      `
    DELETE FROM orders_products WHERE products_id=? AND orders_id=?
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [productId, order.id]
    );

    if (!result.affectedRows) {
      throw generateError("That product was not in the cart.", 404);
    }

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      message: "Product removed from cart.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

<<<<<<< HEAD
//CHECKOUT
=======

>>>>>>> final commit
async function checkOut(req, res, next) {
  let connection;
  try {
    const { userId } = req.auth;
    const { addressId } = req.body;

    connection = await getConnection();

    const [order] = await connection.query(
      `
    SELECT pr.id AS productId, quantity, stock, available, pr.price AS price FROM orders o 
    LEFT JOIN orders_products op ON o.id=op.orders_id 
    LEFT JOIN products pr ON op.products_id=pr.id WHERE o.users_id=? AND finished=0;
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [userId]
    );

    if (!order.length) {
      throw generateError(`You don't have a current order.`, 404);
    }

    for (let i = order.length - 1; i >= 0; i--) {
      const { productId, quantity, stock, price, available } = order[i];

      if (!available || stock < quantity) {
        throw generateError(
<<<<<<< HEAD
          `Some products are not available or there is not enugh stock. Remove them from your cart.`,
          409
        );
      } else {
        await connection.query(
          `
      UPDATE orders_products SET price=? WHERE products_id=?
      `,
=======
          `Some products are not available or there is not enugh stock. Remove them from your cart.`, 409
          );
      } else {
        await connection.query(
          `
        UPDATE orders_products SET price=? WHERE products_id=?
          `,
>>>>>>> final commit
          [price, productId]
        );
      }
    }

    const [address] = await connection.query(
      `
    SELECT alias, name, street, council FROM addresses WHERE id=? 
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [addressId]
    );

    await connection.query(
      `
    UPDATE orders SET addresses_id=? WHERE users_id=? AND finished=0
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [addressId, userId]
    );

    const totalPrice = order.reduce(
      (accumulator, currentProduct) =>
        accumulator + currentProduct.price * currentProduct.quantity,
      0
    );

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      total: totalPrice,
      address: address[0],
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

<<<<<<< HEAD
=======

>>>>>>> final commit
async function finishedCart(req, res, next) {
  let connection;
  try {
    const { userId } = req.auth;

    connection = await getConnection();

    const [order] = await connection.query(
      `
    SELECT o.id AS orderId, pr.id AS productId, pr.name as productName,
    quantity, stock, u.email AS vendorEmail 
    FROM orders o 
    LEFT JOIN orders_products op ON o.id=op.orders_id 
    LEFT JOIN products pr ON op.products_id=pr.id 
    LEFT JOIN shops s ON pr.shops_id=s.id
    LEFT JOIN users u ON s.users_id = u.id
    WHERE o.users_id=? AND finished=0;
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [userId]
    );

    if (!order.length) {
      throw generateError(`Order not found.`, 404);
    }

    for (let i = order.length - 1; i >= 0; i--) {
      const { vendorEmail, productName, productId, quantity, stock } = order[i];

      let available = 1;

      if (stock === +quantity) {
        available = 0;
      }

      await connection.query(
        `
<<<<<<< HEAD
    UPDATE products SET stock=?, available=? WHERE id=?
    `,
=======
      UPDATE products SET stock=?, available=? WHERE id=?
        `,
>>>>>>> final commit
        [stock - quantity, available, productId]
      );

      const message = `¡Enhorabuena aRRRtesán! Alguien acaba de adquirir ${quantity} de tu(s) <a href="${process.env.FRONT_URL}/product/${productId}">${productName}. ¡Lo celebramos contigo!</a>`;

      await sendEmail({
        email: vendorEmail,
        title: "GRRR — Confirmación de venta",
        message: message,
      });
    }

    const { orderId } = order[0];

    await connection.query(
      `
    UPDATE orders SET finished=1, sell_date=CURRENT_TIMESTAMP WHERE id=?
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [orderId]
    );

    const [user] = await connection.query(
      `
    SELECT email FROM users WHERE id=?
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [userId]
    );

    const { email } = user[0];

    await sendEmail({
      email: email,
      title: "GRRR — Correo de confirmación de compra",
      message: `<h1>Tu compra se ha realizado con éxito, pronto recibirás tu pedido en casa. ¡Gracias mantener vivo el pequeño comercio! Has hecho muy feliz a un aRRRtesán.  </h1>`,
    });

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      message: "Order completed.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

module.exports = {
  showCurrentCart,
  showFinishedOrders,
  addToCart,
  deleteFromCart,
  checkOut,
  finishedCart,
};
