const { getConnection } = require("../db");
const { generateError } = require("../helpers");
const { shopSchema } = require("./validations");

<<<<<<< HEAD
=======
async function listShops(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    let [result] = await connection.query(
      `
    SELECT id, name, description, instagram
    FROM shops 
      `
    );

    res.send({
      status: "Ok",
      results: result.length,
      result,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}


>>>>>>> final commit
async function getShop(req, res, next) {
  let connection;
  try {
    const { shopId } = req.params;
    connection = await getConnection();

    const [resultShop] = await connection.query(
      `
    SELECT name, description, instagram FROM shops WHERE id=? 
    `,
      [shopId]
    );

    const [products] = await connection.query(
      `
    SELECT pr.id, pr.name, s.name AS shopName, price, available, category, type, 
    photo, avg(rating) AS avgRating, COUNT(rating) AS votes from products pr 
    LEFT JOIN ratings r ON pr.id = r.products_id 
        LEFT JOIN shops s ON shops_id = s.id
    WHERE shops_id=? group by pr.id
    `,
      [shopId]
    );

    if (!resultShop.length) {
      throw generateError(`Shop not found`, 404);
    }

    const {
      name,
      description,
      instagram
    } = resultShop[0];

    const payload = {
      name,
      description,
      products,
      instagram
    };
    res.send({
      status:"ok",
      message:payload,
    })
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function createShop(req, res, next) {
<<<<<<< HEAD
  const { userId, role } = req.auth;
  let connection;
  try {
    if (role === "vendor") {
      throw generateError(`You already have a shop.`, 401);
    }
=======
  const { userId} = req.auth;
  let connection;
  try {
>>>>>>> final commit
    await shopSchema.validateAsync(req.body);
    connection = await getConnection();
    const { name, description, instagram } = req.body;

    await connection.query(
      `
  INSERT INTO shops (users_id, name, description, instagram) 
  VALUES (?, ?, ?, ?) 
  `,
      [userId, name, description, instagram]
    );
    await connection.query(
      `
<<<<<<< HEAD
  UPDATE users SET role="vendor", forced_expiration_date=CURRENT_TIMESTAMP WHERE id=?
=======
  UPDATE users SET forced_expiration_date=CURRENT_TIMESTAMP WHERE id=?
>>>>>>> final commit
  `,
      [userId]
    );
    res.send({
      status: "ok",
      message:
        "Shop created succesfully. Login again to get your vendor token.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function editShop(req, res, next) {
  let connection;
  try {
    const { shopId } = req.params;
    await shopSchema.validateAsync(req.body);

    const { name, description, instagram } = req.body;

    connection = await getConnection();

    if (Number(shopId) !== req.auth.shopId && req.auth.role !== "admin") {
      throw generateError("You have not permission to edit this shop.");
    }

    const [result] = await connection.query(
      `
          UPDATE shops SET name=?, description=?, instagram=? WHERE id=?
          `,
      [name, description, instagram, shopId]
    );

    if (result.affectedRows === 0) {
      throw generateError(`The shop with id ${shopId} does not exist`, 404);
    }
    res.send({
      status: "ok",
      message: "Shop updated successfully.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function promoteShop(req, res, next) {
  let connection;
  try {
    const { shopId } = req.params;

    connection = await getConnection();

    const [result] = await connection.query(
      `
      UPDATE shops SET promoted=1 WHERE id=?
      `,
      [shopId]
    );

    if (result.affectedRows === 0) {
      throw generateError("Shop not found", 404);
    }

    res.send({
      status: "ok",
      message: `Shop ${shopId} is now promoted.`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

async function unpromoteShop(req, res, next) {
  let connection;
  try {
    const { shopId } = req.params;

    connection = await getConnection();

    const result = await connection.query(
      `
    UPDATE shops SET promoted=0 WHERE id=?
    `,
      [shopId]
    );

    if (result.affectedRows === 0) {
      throw generateError("Shop not found", 404);
    }

    res.send({
      status: "ok",
      message: `Shop ${shopId} is not promoted anymore.`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}


async function deleteShop(req, res, next) {
  let connection;
  try {
    const { shopId } = req.params;
    if (Number(shopId) !== req.auth.shopId && req.auth.role !== "admin") {
      throw generateError("You have not permission to edit this shop.");
    }

    connection = await getConnection();

    const [result] = await connection.query(
      `
    SELECT users_id AS userId FROM shops WHERE id=?
    `,
      [shopId]
    );

    const [owner] = result;

    if (!owner) {
      throw generateError(`Shop not found`, 404);
    }

    await connection.query(
      `
DELETE FROM shops WHERE id=?
`,
      [shopId]
    );

    await connection.query(
      `
UPDATE users SET role='regular', forced_expiration_date=CURRENT_TIMESTAMP WHERE id=?
`,
      [owner.userId]
    );

    res.send({
      status: "ok",
      message: `Shop ${shopId} deleted. Login again to get a new token.`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

module.exports = {
<<<<<<< HEAD
=======
  listShops,
>>>>>>> final commit
  getShop,
  createShop,
  editShop,
  promoteShop,
  unpromoteShop,
  deleteShop,
<<<<<<< HEAD
=======
  
>>>>>>> final commit
};
