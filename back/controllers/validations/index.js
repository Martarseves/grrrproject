const Joi = require("@hapi/joi");
const JoiAge = Joi.extend(require("joi-age"));

const { generateError, categories} = require("../../helpers");


const emailSchema = Joi.string()
  .email()
  .required()
  .error(generateError("Invalid email.", 400));

const passwordSchema = Joi.string()
  .min(8)
  .max(20)
  .required()
  .error(
    generateError(
      "The password must contain more than 8 and less tahn 20 characters.",
      400
    )
  );

const registerUserSchema = Joi.object().keys({
  email: emailSchema,
  password: passwordSchema,
});

const loginUserSchema = Joi.object().keys({
  email: emailSchema,
  password: passwordSchema,
});

const editUserSchema = Joi.object().keys({
  firstName: Joi.string()
    .max(50)
    .error(
      generateError("First name cannot be longer than 50 characters.", 400)
    ),
  lastName: Joi.string()
    .max(50)
    .error(
      generateError("Last name can not be longer than 50 characters.", 400)
    ),
});

const editPasswordUserSchema = Joi.object().keys({
  oldPassword: passwordSchema,
  newPassword: passwordSchema,
});


const addressSchema = Joi.object().keys({
  alias: Joi.string()
    .max(255)
    .required()
    .error(generateError("Alias cannot be longer than 255 characters.", 400)),
  name: Joi.string()
    .max(255)
    .required()
    .error(generateError("Name cannot be longer than 255 characters.", 400)),
  
  street: Joi.string()
    .max(255)
    .error(generateError("Street cannot be longer than 255 characters.", 400)),
  
  council: Joi.string()
    .max(100)
    .required()
    .error(
      generateError("Council cannot be longer than 100 characters.", 400)
    ),

  addressId: Joi.number(),
});

const shopSchema = Joi.object().keys({
  name: Joi.string()
    .max(100)
    .error(generateError("Name cannot be longer than 100 characters.", 400)),
  description: Joi.string()
    .max(500)
    .error(
      generateError("Description cannot be longer than 500 characters.", 400)
    ),
  instagram: Joi.string().uri().trim(),
});

const productSchema = Joi.object().keys({
  category: Joi.string()
    .valid(...categories)
    .required()
    .error(generateError("Select a valid category", 400)),
  name: Joi.string()
    .max(100)
    .required()
    .error(generateError("Name cannot be longer than 100 characters", 400)),
<<<<<<< HEAD
  price: Joi.number()
    .positive()
    .max(99999)
    .required()
    .error(
      generateError("Price cannot be higher than 99999", 400)),
  stock: Joi.number()
    .integer()
    .positive()
    .max(1000)
    .required()
    .error(
      generateError("Sotck has to be under 1000", 400)),
  available: Joi.binary()
    .max(1)
    .default(0)
    .error(generateError("You must select a valid availability option", 400)),
  type: Joi.string()
    .valid("available", "custom")
    .default("ready")
    .error(generateError("Select a valid type option", 400)),
=======
  price: Joi.string(),
  //   .positive()
  //   .max(100)
  //   .required()
  //   .error(
  //     generateError("Price cannot be higher than 1000", 400)),
  stock: Joi.string(),
  //   .integer()
  //   .positive()
  //   .max(1000)
  //   .required()
  //   .error(
  //     generateError("Stock has to be under 1000", 400)),
  available: Joi.binary(),
  //   .max(1)
  //   .default(0)
  //   .error(generateError("You must select a valid availability option", 400)),
>>>>>>> final commit
  description: Joi.string()
    .max(500)
    .error(
      generateError("Description cannot be longer than 500 characters.", 400)),
});

const rateSchema = Joi.object().keys({
  rating: Joi.number()
    .positive()
    .precision(1)
    .max(5)
    .required()
    .error(generateError("You must vote from 1 to 5", 400)),
  comment: Joi.string()
    .max(255)
    .error(
      generateError("This field cannot be longer than 255 characters", 400)
    ),
});

module.exports = {
  emailSchema,
  registerUserSchema,
  loginUserSchema,
  editUserSchema,
  editPasswordUserSchema,
  shopSchema,
  productSchema,
  rateSchema,
<<<<<<< HEAD
  // searchSchema,
=======
>>>>>>> final commit
  addressSchema,
};
