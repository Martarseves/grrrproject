const { getConnection } = require("../db");
const {
  generateError,
  processAndSavePhoto,
  deletePhoto,
} = require("../helpers");
const { productSchema, rateSchema } = require("./validations");

<<<<<<< HEAD
////GET PRODUCT INFO////
=======

>>>>>>> final commit
async function getProduct(req, res, next) {
  let connection;

  try {
    const { productId } = req.params;
    connection = await getConnection();
    const [result] = await connection.query(
<<<<<<< HEAD

      `
    SELECT pr.name, pr.description, price, category, available, type, photo, avg(rating) AS avgRating, COUNT(rating) AS votes, s.id AS shopId, s.name AS shopName  from products pr LEFT JOIN ratings r ON pr.id = r.products_id LEFT JOIN shops s ON pr.shops_id = s.id WHERE pr.id=? group by pr.id;
    `,
=======
      `
    SELECT pr.name, pr.description, price, category, available, type, photo, avg(rating) AS avgRating, COUNT(rating) AS votes, s.id AS shopId, s.name AS shopName  from products pr LEFT JOIN ratings r ON pr.id = r.products_id LEFT JOIN shops s ON pr.shops_id = s.id WHERE pr.id=? group by pr.id;
      `,
>>>>>>> final commit
      [productId]
    );

    if (!result.length) {
      throw generateError(`Product not found.`, 404);
    }

    const [product] = result;

    const [ratings] = await connection.query(
      `
    SELECT rating, comment FROM ratings WHERE products_id=?
<<<<<<< HEAD
    `,
      [productId]
    );

    //Get related products
=======
      `,
      [productId]
    );
  

    //GET RELATED PRODUCTS
>>>>>>> final commit
    const { category } = product;
    const [relatedProducts] = await connection.query(
      `
    SELECT pr.id, pr.name, s.name AS shopName, category, price, available, type, 
<<<<<<< HEAD
    photo, avg(rating) AS avgRating, COUNT(rating) AS votes from products pr 
    LEFT JOIN ratings r ON pr.id = r.products_id 
        LEFT JOIN shops s ON shops_id = s.id
    WHERE AVAILABLE=1 AND category=? AND NOT pr.id=? group by pr.id 
    `,
=======
      photo, avg(rating) AS avgRating, COUNT(rating) AS votes from products pr 
    LEFT JOIN ratings r ON pr.id = r.products_id 
    LEFT JOIN shops s ON shops_id = s.id
    WHERE AVAILABLE=1 AND category=? AND NOT pr.id=? group by pr.id 
      `,
>>>>>>> final commit
      [category, productId]
    );

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      product,
      ratings,
      relatedProducts,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

<<<<<<< HEAD
////ADD NEW PRODUCT
async function newProduct(req, res, next) {
  const { shopId } = req.auth;

  let connection;
  try {
    if (!shopId) {
      throw generateError(`You do not have a shop.`, 401);
    }
=======

async function newProduct(req, res, next) {
  const { userId } = req.auth;

  let connection;
  try {
    if (!userId) {
      throw generateError(`You do not have a shop.`, 401);
    }
    console.log(req.body);
>>>>>>> final commit
    await productSchema.validateAsync(req.body);
    connection = await getConnection();
    const {
      name,
      description,
      category,
      price,
      stock,
      available,
     
    } = req.body;

    let savedFileName = null;
    if (req.files && req.files.photo) {
      try {
        savedFileName = await processAndSavePhoto(req.files.photo);
      } catch (error) {
        throw generateError("Can not process upload image. Try again later.");
      }
    }

<<<<<<< HEAD
 
    const [product] = await connection.query(
      `
  INSERT INTO products (shops_id, name, description, category, price, stock, available, photo) 
  VALUES (?, ?, ?, ?, ?, ?, ?, ? ) 
  `,
      [
        shopId,
=======


    const [selectUserShop]= await connection.query(

      `SELECT id FROM shops
      WHERE shops.users_id = ?`,
      [userId]
    );
    console.log(selectUserShop)

    const userShop=selectUserShop[0].id;
    console.log(userShop);

    const [product] = await connection.query(
      `
    INSERT INTO products (shops_id, name, description, category, price, stock, available, photo) 
    VALUES (?, ?, ?, ?, ?, ?, ?,? ) 
      `,
      [
        userShop,
>>>>>>> final commit
        name,
        description,
        category,
        price,
        stock,
        available,
        savedFileName,
      ]
    );
<<<<<<< HEAD

    res.send({
      status: "ok",
=======
    res.send({
      status: "Ok",
>>>>>>> final commit
      message: "Product added succesfully.",
      productId: product.insertId,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

<<<<<<< HEAD
////EDIT PRODUCT
=======

>>>>>>> final commit
async function editProduct(req, res, next) {
  const { shopId, role } = req.auth;
  const { productId } = req.params;

  let connection;
  try {
    if (!shopId) {
      throw generateError(`You do not have a shop.`, 401);
    }
    await productSchema.validateAsync(req.body);
    connection = await getConnection();
    const {
      name,
      description,
      category,
      price,
      stock,
      available,
    } = req.body;

<<<<<<< HEAD
    //Check if user has permission
=======
    
>>>>>>> final commit
    const [result] = await connection.query(
      `
  SELECT shops_id, photo FROM products WHERE id=?
  `,
      [productId]
    );

    const [product] = result;
    if (!product) {
      throw generateError(`Product not found`, 404);
    }

    if (shopId !== product.shops_id && role !== "admin") {
      throw generateError(`You can not edit this product.`, 401);
    }

    let savedFileName;

    if (req.files && req.files.photo) {
      try {
        savedFileName = await processAndSavePhoto(req.files.photo);
        if (product.photo) {
          await deletePhoto(product.photo);
        }
      } catch (error) {
        throw generateError("Can not process upload image. Try again later.");
      }
    } else {
      savedFileName = product.photo;
    }

<<<<<<< HEAD
    //Update product

=======
>>>>>>> final commit

    await connection.query(
      `
UPDATE products SET name=?, description=?, category=?, price=?, stock=?, available=?, photo=? WHERE id=?
`,
      [
        name,
        description,
        category,
        price,
        stock,
        available,
<<<<<<< HEAD
                savedFileName,
=======
        savedFileName,
>>>>>>> final commit
        productId,
      ]
    );

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      message: "Product updated.",
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

<<<<<<< HEAD
////DELETE PRODUCT
=======

>>>>>>> final commit
async function deleteProduct(req, res, next) {
  let connection;
  try {
    const { shopId, role } = req.auth;
    const { productId } = req.params;
    connection = await getConnection();

    //Check if user has permission
    const [result] = await connection.query(
      `
<<<<<<< HEAD
  SELECT shops_id FROM products WHERE id=?
  `,
=======
    SELECT shops_id FROM products WHERE id=?
      `,
>>>>>>> final commit
      [productId]
    );

    const [product] = result;
    if (!product) {
      throw generateError(`Product not found`, 404);
    }

    if (shopId !== product.shops_id && role !== "admin") {
      throw generateError(`You can not edit this product.`, 401);
    }

    await connection.query(
      `
<<<<<<< HEAD
DELETE FROM products WHERE id=?
`,
=======
    DELETE FROM products WHERE id=?
      `,
>>>>>>> final commit
      [productId]
    );

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      message: `Product ${productId} deleted.`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

<<<<<<< HEAD
////LIST PRODUCTS
=======

>>>>>>> final commit
async function listProducts(req, res, next) {
  let connection;
  try {
    connection = await getConnection();
    let [result] = await connection.query(
      `
    SELECT pr.id, pr.name, s.name AS shopName, pr.description, price, available, 
<<<<<<< HEAD
    category, type, photo, avg(rating) AS avgRating, COUNT(rating) AS votes 
    from products pr 
    LEFT JOIN ratings r ON pr.id = r.products_id 
        LEFT JOIN shops s ON shops_id = s.id
    group by pr.id
    `
=======
      category, type, photo, avg(rating) AS avgRating, COUNT(rating) AS votes 
      from products pr 
    LEFT JOIN ratings r ON pr.id = r.products_id 
    LEFT JOIN shops s ON shops_id = s.id group by pr.id
      `
>>>>>>> final commit
    );

    let [categories] = await connection.query(`
    SELECT DISTINCT category FROM products`);

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      results: result.length,
      result,
      categories,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

<<<<<<< HEAD
////RATE PRODUCT
=======

>>>>>>> final commit
async function rateProduct(req, res, next) {
  let connection;
  try {
    if (!req.auth) {
      throw generateError(`You must be logged.`, 401);
    }
    await rateSchema.validateAsync(req.body);
    const { rating, comment } = req.body;
    const { userId } = req.auth;
    const { productId } = req.params;

    connection = await getConnection();
<<<<<<< HEAD

    //Check if user already rated the product
    const [result] = await connection.query(
      `
    SELECT id FROM ratings WHERE users_id=? AND products_id=?
    `,
=======
    
    const [result] = await connection.query(
      `
    SELECT id FROM ratings WHERE users_id=? AND products_id=?
      `,
>>>>>>> final commit
      [userId, productId]
    );

    if (result.length) {
      throw generateError(`You have already rated this product.`, 401);
    }

    await connection.query(
      `
    INSERT INTO ratings (users_id, products_id, rating, comment)
    VALUES (?, ?, ?, ?)
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [userId, productId, rating, comment]
    );

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      message: `You voted this product with ${rating} stars.`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

<<<<<<< HEAD
=======

>>>>>>> final commit
async function editRatingProduct(req, res, next) {
  let connection;
  try {
    if (!req.auth) {
      throw generateError(`You must be logged.`, 401);
    }
    await rateSchema.validateAsync(req.body);
    const { rating, comment } = req.body;
    const { userId } = req.auth;
    const { productId } = req.params;

    connection = await getConnection();

    const [result] = await connection.query(
      `
    UPDATE ratings SET rating=?, comment=? WHERE users_id=? AND products_id=?
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [rating, comment, userId, productId]
    );

    if (result.affectedRows === 0) {
      throw generateError("You did not rate this product yet.", 400);
    }

    res.send({
<<<<<<< HEAD
      status: "ok",
=======
      status: "Ok",
>>>>>>> final commit
      message: `You changed your vote to ${rating} stars.`,
    });
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

module.exports = {
  getProduct,
  newProduct,
  editProduct,
  deleteProduct,
  listProducts,
  rateProduct,
  editRatingProduct,
};
