require("dotenv").config();
const jwt = require("jsonwebtoken");

const { getConnection } = require("../db");
const { generateError } = require("../helpers");

async function isAuth(req, res, next) {
  let connection;

  try {
    const { authorization } = req.headers;
    if (!authorization) {
      throw generateError("Authorization header is missing.");
    }

    const authorizationParts = authorization.split(" ");

    let token;

    if (authorizationParts.length === 1) {
      token = authorization;
    } else if (authorizationParts[0] === `Bearer`) {
      token = authorizationParts[1];
    } else {
      throw generateError("Can not read token.");
    }

    let decoded;

    try {
      decoded = jwt.verify(token, process.env.SECRET);
    } catch (error) {
      throw generateError("Wrong token.");
    }

    const { userId, iat } = decoded;

    connection = await getConnection();

    const [
      result,
    ] = await connection.query(
<<<<<<< HEAD
      "SELECT forced_expiration_date, email FROM users WHERE id=?",
=======
      `SELECT forced_expiration_date, email FROM users WHERE id=?`,
>>>>>>> final commit
      [userId]
    );

    const [user] = result;

    if (!user) {
      throw generateError("There is no user with such id in the database.");
    }

    const forcedExpirationUnixTime = new Date(
      user.forced_expiration_date
    ).getTime();

<<<<<<< HEAD
    if (new Date(iat * 1000) < forcedExpirationUnixTime) {
      throw generateError("Expired token. Login to get a new one.");
    }

=======
    // if (new Date(iat * 1000) < forcedExpirationUnixTime) {
    //   throw generateError("Expired token. Login to get a new one.");
    // }
>>>>>>> final commit
    req.auth = decoded;
    next();
  } catch (error) {
    error.httpCode = 401;
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

function isAdmin(req, res, next) {
  if (!req.auth || req.auth.role !== "admin") {
    return next(generateError("You do not have admin privileges.", 401));
  }
<<<<<<< HEAD

=======
>>>>>>> final commit
  next();
}

async function isVendor(req, res, next) {
  if (req.auth.role === "admin") {
    next();
  }
  if (!req.auth || req.auth.role !== "vendor") {
<<<<<<< HEAD
    return next(generateError("You do not have a shop.", 401));
=======
    return next(generateError("You do not own a shop.", 401));
>>>>>>> final commit
  }

  let connection;
  try {
    const { userId } = req.auth;

    connection = await getConnection();

    const [result] = await connection.query(
      `
    SELECT id FROM shops WHERE users_id=?
<<<<<<< HEAD
    `,
=======
      `,
>>>>>>> final commit
      [userId]
    );

    const [shop] = result;

    if (!shop) {
      throw generateError(`Shop not found.`, 404);
    }

    req.auth.shopId = shop.id;

    next();
  } catch (error) {
    next(error);
  } finally {
    if (connection) {
      connection.release();
    }
  }
}

module.exports = {
  isAuth,
  isAdmin,
  isVendor,
};
