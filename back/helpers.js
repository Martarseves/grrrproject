require("dotenv").config();

const { format } = require("date-fns");
const crypto = require("crypto");
const sgMail = require("@sendgrid/mail");
const sharp = require("sharp");
const fs = require("fs-extra");
const path = require("path");
const uuid = require("uuid");

const imageUploadPath = path.join(__dirname, process.env.UPLOADS_DIR);

const categories = [
  "Menaje",
  "Cocina",
  "Higiene",
  "Otro",
];

function formatDateToDB(date) {
  return format(date, "yyyy-MM-dd HH:mm:ss");
}

function generateError(message, code) {
  const error = new Error(message);
  if (code) error.httpCode = code;
  return error;
}

function randomString(size = 20) {
  return crypto.randomBytes(size).toString("hex").slice(0, size);
}

async function sendEmail({ email, title, message }) {
  try {
    sgMail.setApiKey(process.env.SENDGRID_KEY);

    const msg = {
      to: email,
      from: process.env.SENDGRID_FROM,
      subject: title,
      html: message,
    };
    console.log("email sent successfully");
    await sgMail.send(msg);
  } catch (error) {
    throw generateError(`Error sending the email.`);
  }
}

async function processAndSavePhoto(uploadedImage) {
  const savedFileName = `${uuid.v1()}.jpg`;

  await fs.ensureDir(imageUploadPath);

  const finalImage = sharp(uploadedImage.data);

  const imageInfo = await finalImage.metadata();

  if (imageInfo.width > 500) {
    finalImage.resize(500);
  }

  await finalImage.toFile(path.join(imageUploadPath, savedFileName));

  return `${process.env.PUBLIC_HOST}/uploads/${savedFileName}`;
}

async function deletePhoto(imagePath) {
  const fileName = imagePath.split("/").reverse()[0];
  await fs.unlink(path.join(imageUploadPath, imagePath));
}

async function getAndSendVerificationCode(email, change) {
  const verificationCode = randomString(40);
  
  let validationURL = `${process.env.VUE_APP_API_URL}/#/validate?code=${verificationCode}`;
  console.log(validationURL);
  if (change) {
    validationURL = `${process.env.VUE_APP_API_URL}/#/validate?code=${verificationCode}&email=${email}`;
  }

  try {
    await sendEmail({
      email: email,
      title: "GRRR — Correo de confirmación de cuenta",
<<<<<<< HEAD
      message: `<h1> ¡Holi! Ya te queda poco para formar parte de nuestra plantilla de aRRRtesáns, solamente tienes que hacer click <a href='${validationURL}'>aquí</a> para confirmar tu cuenta. ¡Nos vemos pronto! </h1>
=======
      message: `<h1> ¡Hola! Ya te queda poco para formar parte de nuestra plantilla de aRRRtesáns, solamente tienes que hacer click <a href='${validationURL}'>aquí</a> para confirmar tu cuenta. ¡Nos vemos pronto! </h1>
>>>>>>> final commit
    `,
    });
  } catch (error) {
    throw generateError(
      "Error sending the confirmation email. Try again later."
    );
  }
  return verificationCode;
}

module.exports = {
  formatDateToDB,
  generateError,
  randomString,
  sendEmail,
  processAndSavePhoto,
  deletePhoto,
  getAndSendVerificationCode,
  categories,
};
