require("dotenv").config();

const { getConnection } = require("./db");
const { formatDateToDB, categories} = require("./helpers");
const bcrypt = require("bcrypt");

const faker = require("faker/locale/es");
const { random, shuffle, sample, sampleSize } = require("lodash");

const addData = process.argv[2] === "--data";

async function main() {
  const connection = await getConnection();

  console.log("Dropping tables");
  await connection.query("DROP TABLE IF EXISTS orders_products");
  await connection.query("DROP TABLE IF EXISTS wishlists");
  await connection.query("DROP TABLE IF EXISTS ratings");
  await connection.query("DROP TABLE IF EXISTS orders");
  await connection.query("DROP TABLE IF EXISTS products");
  await connection.query("DROP TABLE IF EXISTS shops");
  await connection.query("DROP TABLE IF EXISTS addresses");
  await connection.query("DROP TABLE IF EXISTS users");


  await connection.query(`
  CREATE TABLE users (
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  email VARCHAR(50) NOT NULL UNIQUE,
  photo VARCHAR(255),
  first_name VARCHAR(50),
  last_name VARCHAR(50),
  password VARCHAR(255),
  forced_expiration_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
<<<<<<< HEAD
  role ENUM('regular', 'vendor', 'admin') DEFAULT 'regular' NOT NULL,
  creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  active BOOLEAN DEFAULT false NOT NULL,
=======
  role ENUM('regular', 'admin') DEFAULT 'regular' NOT NULL,
  creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  active TINYINT DEFAULT '0' NOT NULL,
>>>>>>> final commit
  verification_code VARCHAR(255)
  );
  `);

  await connection.query(`
  CREATE TABLE addresses (
<<<<<<< HEAD
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
users_id INT UNSIGNED,
alias VARCHAR(255),
name VARCHAR(255),
street VARCHAR(255),
council VARCHAR(100),
creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
CONSTRAINT FK_addresses_users FOREIGN KEY (users_id) REFERENCES users(id)
);
=======
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  users_id INT UNSIGNED,
  alias VARCHAR(255),
  name VARCHAR(255),
  street VARCHAR(255),
  council VARCHAR(100),
  creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT FK_addresses_users FOREIGN KEY (users_id) REFERENCES users(id)
  );
>>>>>>> final commit
  `);

  await connection.query(`
  CREATE TABLE shops (
<<<<<<< HEAD
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
users_id INT UNSIGNED UNIQUE,
name VARCHAR(100),
description TEXT(500),
instagram VARCHAR(255),
promoted BOOLEAN DEFAULT false,
creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
CONSTRAINT FK_shops_users FOREIGN KEY (users_id) REFERENCES users(id)
);
=======
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  users_id INT UNSIGNED UNIQUE,
  name VARCHAR(100),
  description TEXT(500),
  instagram VARCHAR(255),
  promoted TINYINT DEFAULT '0'',
  creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT FK_shops_users FOREIGN KEY (users_id) REFERENCES users(id)
  );
>>>>>>> final commit
  `);

  await connection.query(`
  CREATE TABLE products (
<<<<<<< HEAD
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
shops_id INT UNSIGNED,
category VARCHAR(50),
photo LONGBLOB,
name VARCHAR(100),
price DECIMAL(7,2),
stock SMALLINT,
available BOOLEAN DEFAULT false,
type ENUM ('ready', 'custom') NOT NULL DEFAULT 'ready',
description TEXT(500),
creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
CONSTRAINT FK_products_shops FOREIGN KEY (shops_id) REFERENCES shops(id)
);
=======
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  shops_id INT UNSIGNED,
  category VARCHAR(50),
  photo VARCHAR(255),
  name VARCHAR(100),
  price DECIMAL(7,2),
  stock SMALLINT,
  available TINYINT DEFAULT '1',
  type ENUM ('ready', 'custom') NOT NULL DEFAULT 'ready',
  description TEXT(500),
  creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT FK_products_shops FOREIGN KEY (shops_id) REFERENCES shops(id)
  );
>>>>>>> final commit
  `);

  await connection.query(`
  CREATE TABLE orders (
<<<<<<< HEAD
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
users_id INT UNSIGNED,
addresses_id INT UNSIGNED,
finished BOOLEAN DEFAULT FALSE,
sell_date DATETIME,
creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
CONSTRAINT FK_oreders_users FOREIGN KEY (users_id) REFERENCES users(id) ON DELETE SET NULL,
CONSTRAINT FK_oreders_addresses FOREIGN KEY (addresses_id) REFERENCES addresses(id) ON DELETE SET NULL
);
=======
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  users_id INT UNSIGNED,
  addresses_id INT UNSIGNED,
  finished TINYINT DEFAULT '0',
  sell_date DATETIME,
  creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT FK_oreders_users FOREIGN KEY (users_id) REFERENCES users(id) ON DELETE SET NULL,
  CONSTRAINT FK_oreders_addresses FOREIGN KEY (addresses_id) REFERENCES addresses(id) ON DELETE SET NULL
  );
>>>>>>> final commit
  `);

  await connection.query(`
  CREATE TABLE orders_products (
<<<<<<< HEAD
id INT PRIMARY KEY AUTO_INCREMENT,
orders_id INT UNSIGNED,
products_id INT UNSIGNED,
price DECIMAL(7,2),
quantity SMALLINT DEFAULT 1,
CONSTRAINT FK_oreders_products_orders FOREIGN KEY (orders_id) REFERENCES orders(id),
CONSTRAINT FK_orders_products_products FOREIGN KEY (products_id) REFERENCES products(id)
);
=======
  id INT PRIMARY KEY AUTO_INCREMENT,
  orders_id INT UNSIGNED,
  products_id INT UNSIGNED,
  price DECIMAL(7,2),
  quantity SMALLINT DEFAULT 1,
  CONSTRAINT FK_orders_products_orders FOREIGN KEY (orders_id) REFERENCES orders(id),
  CONSTRAINT FK_orders_products_products FOREIGN KEY (products_id) REFERENCES products(id)
  );
>>>>>>> final commit
  `);

  await connection.query(`
  CREATE TABLE wishlists (
<<<<<<< HEAD
id INT PRIMARY KEY AUTO_INCREMENT,
users_id INT UNSIGNED,
products_id INT UNSIGNED,
creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
CONSTRAINT FK_wishlist_users FOREIGN KEY (users_id) REFERENCES users(id),
CONSTRAINT FK_wishlist_products FOREIGN KEY (products_id) REFERENCES products(id)
);

=======
  id INT PRIMARY KEY AUTO_INCREMENT,
  users_id INT UNSIGNED,
  products_id INT UNSIGNED,
  creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  mod_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT FK_wishlist_users FOREIGN KEY (users_id) REFERENCES users(id),
  CONSTRAINT FK_wishlist_products FOREIGN KEY (products_id) REFERENCES products(id)
  );
>>>>>>> final commit
  `);

  await connection.query(`
  CREATE TABLE ratings (
<<<<<<< HEAD
id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
users_id INT UNSIGNED,
products_id INT UNSIGNED,
rating DECIMAL (2,1),
comment VARCHAR(255),
CONSTRAINT FK_ratings_users FOREIGN KEY (users_id) REFERENCES users(id),
CONSTRAINT FK_rating_products FOREIGN KEY (products_id) REFERENCES products(id)
);

=======
  id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  users_id INT UNSIGNED,
  products_id INT UNSIGNED,
  rating DECIMAL (2,1),
  comment VARCHAR(255),
  CONSTRAINT FK_ratings_users FOREIGN KEY (users_id) REFERENCES users(id),
  CONSTRAINT FK_rating_products FOREIGN KEY (products_id) REFERENCES products(id)
  );
>>>>>>> final commit
  `);

  const password = await bcrypt.hash(process.env.DEFAULT_ADMIN_PASSWORD, 10);

<<<<<<< HEAD
  await connection.query(`
  INSERT INTO users(email, password, role, active)
  VALUES('admin@admin.com', '${password}', 'admin', 1 )
  `);

=======
  //CREATE ADMIN
  await connection.query(`
  INSERT INTO users(email, password, role, active)
  VALUES('martseves@gmail.com', '12345678', 'admin', 1 )
  `);


  //GENERATED DDBB DATA:

>>>>>>> final commit
  if (addData) {
    const password = await bcrypt.hash("password", 10);

    await connection.query(`
  INSERT INTO users(email, password, active, first_name, last_name, photo
  VALUES('user@user.com', '${password}', 1, '${faker.name.firstName()}', '${faker.name.lastName()}', '${faker.internet.avatar()}')
  `);

    //Users
    const users = 50;

    for (let i = 0; i < users; i++) {
      const password = await bcrypt.hash(faker.internet.password(), 10);

      await connection.query(`
  INSERT INTO users(email, password, active, first_name, last_name, photo)
  VALUES('${faker.internet.email()}', '${password}', 1, '${faker.name.firstName()}', '${faker.name.lastName()}', '${faker.internet.avatar()}')
  `);
    }

    //Addresses
    const extraAddresses = 50;
    for (let i = 0; i <= users; i++) {
      await connection.query(`
      INSERT INTO addresses(users_id, alias, name, street,council
      VALUES ('${i + 2}', '${faker.lorem.words(
        2
      )}', '${faker.name.findName()}', '${faker.address.street()}',
     '${faker.address.council()}')
      `);
    }

    //Extra addresses
    for (let i = 0; i < extraAddresses; i++) {
      await connection.query(`
      INSERT INTO addresses(users_id, alias, name,street, council)
      VALUES ('${random(2, 12)}', '${faker.lorem.words(
        2
      )}', '${faker.name.findName()}', '${faker.address.street()}', 
       '${faker.address.council()}', )
      `);
    }

    //Shops
    const shops = users / 2;
    const randomUserIds = shuffle([...Array(users).keys()]);

    for (let i = 0; i < shops; i++) {
      const userId = randomUserIds[i] + 3;
      const name = faker.lorem.words(random(1, 3));
      const rssName = name.replace(/ /g, "");
      const promoted = Math.random() > 0.75 ? 1 : 0;

      await connection.query(`
      INSERT INTO shops(users_id, name, description, instagram, promoted)
      VALUES('${userId}', '${name}', '${faker.lorem.paragraph()}', 'https://www.instagram.com/${rssName}', 
      ${promoted})`);

      await connection.query(`
          UPDATE users SET role = 'vendor' WHERE id = ${userId}
          `);
    }

    //Productos
    const products = 50;

    for (let i = 0; i < products; i++) {
      const stock = sample([0, random(1, 5), random(6, 50), random(51, 500)]);
      const available = stock ? 1 : 0;
      await connection.query(`
      INSERT INTO products(shops_id, category, name, price, stock, available, type, description, photo)
      VALUES ('${random(1, shops)}', '${sample(
        categories
      )}', '${faker.lorem.words(random(1, 5))}', '${random(1000, true).toFixed(
        2
      )}', '${stock}', '${available}', '${sample([
        "ready",
        "custom",
      ])}', '${faker.lorem.paragraph()}', '${process.env.PUBLIC_HOST}/uploads/product${i}.jpg')
      `);
    }

    //Ratings
    const ratings = 200;

    for (let i = 0; i < ratings; i++) {
      const userId = random(2, users + 2);
      const productId = random(1, products);

      await connection.query(`
      INSERT INTO ratings(users_id, products_id, rating, comment)
      VALUES ('${userId}', '${productId}', '${random(
        0,
        5
      )}', '${faker.lorem.sentence()}')`);
    }

    //Finished orders
    const finishedOrders = 50;

    for (let i = 0; i < finishedOrders; i++) {
      const userId = random(2, users + 2);
      const [userAddresses] = await connection.query(`
      SELECT id FROM addresses WHERE users_id = '${userId}'
      `);

      const addressId = sample(userAddresses).id;
      const sellDate = formatDateToDB(faker.date.past());

      await connection.query(`
      INSERT INTO orders(users_id, addresses_id, finished, sell_date)
      VALUES ('${userId}', '${addressId}', '1', '${sellDate}')`);

      const productsPerOrder = 3;

      for (let j = 0; j < productsPerOrder; j++) {
        await connection.query(`
      INSERT INTO orders_products(orders_id, products_id, price, quantity)
      VALUES ('${i + 1}', '${random(1, products)}', '${random(
          1000,
          true
        ).toFixed(2)}', '${random(1, 10)}')`);
      }
    }

    //Wishlist
    const listedProducts = 200;

    for (let i = 0; i < listedProducts; i++) {
      const userId = random(2, users + 2);

      await connection.query(`
      INSERT INTO wishlists(users_id, products_id)
      VALUES ('${userId}', '${random(1, products)}')`);
    }

    console.log("Example data added");
  }

  console.log("Initial structure created");

  connection.release();
  process.exit();
}

main();
