require("dotenv").config();

const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const path = require("path");

<<<<<<< HEAD
const {isAuth, isAdmin, isVendor} = require("./middlewares/auth");
=======
const {isAuth, isAdmin} = require("./middlewares/auth");
>>>>>>> final commit

const app = express();

const { getIndex } = require("./controllers");

const {
  registerUser,
  loginUser,
  getInfoUser,
  editUser,
  updatePasswordUser,
  updateEmailUser,
  validateUser,
  validateEmailUser,
  resendVerificationEmail,
  deleteUser,
} = require("./controllers/users");

const {
  listAddress,
  addAddress,
  editAddress,
  deleteAddress,
} = require("./controllers/addresses");

const {
<<<<<<< HEAD
=======
  listShops,
>>>>>>> final commit
  getShop,
  createShop,
  editShop,
  promoteShop,
  unpromoteShop,
  deleteShop,
} = require("./controllers/shops");

const {
  getProduct,
  newProduct,
  editProduct,
  deleteProduct,
  listProducts,
  rateProduct,
  editRatingProduct,
} = require("./controllers/products");

const {
  listWishlist,
  addToWishlist,
  deleteFromWishlist,
} = require("./controllers/wishlists");

const {
  showCurrentCart,
  showFinishedOrders,
  addToCart,
  deleteFromCart,
  checkOut,
  finishedCart
} = require("./controllers/orders");


app.use(morgan("dev"));
app.use(cors());
app.use(bodyParser.json());
app.use(fileUpload());
app.use(express.static(path.join(__dirname, "static")));
app.get("/", getIndex);

//USERS
//register
app.post("/users", registerUser);
//login
app.post("/users/login", loginUser);
//resend verification email
app.post("/users/resendmail", resendVerificationEmail);
//validate user
app.get("/users/validate", validateUser);
//validate user email
app.get("/users/validateEmail", validateEmailUser);
//see user info
app.get("/users/:userId", isAuth, getInfoUser);
//edit user
app.put("/users/:userId", isAuth, editUser);
//edit user password
app.put("/users/:userId/password", isAuth, updatePasswordUser);
//edit user email
app.put("/users/:userId/email", isAuth, updateEmailUser);
//delete user
app.delete("/users/:userId", isAuth, deleteUser);

//ADDRESSES
//list addresses
app.get("/addresses", isAuth, listAddress);
//add address
app.post("/addresses", isAuth, addAddress);
//edit address
app.put("/addresses/:addressId", isAuth, editAddress);
//delete address
app.delete("/addresses/:addressId", isAuth, deleteAddress);

//PRODUCTS
//list products
app.get("/products", listProducts);
//see product info
app.get("/products/:productId", getProduct);
//add product
<<<<<<< HEAD
app.post("/products", isAuth, isVendor, newProduct);
//edit product
app.put("/products/:productId", isAuth, isVendor, editProduct);
//delete product
app.delete("/products/:productId", isAuth, isVendor, deleteProduct);
=======
app.post("/products", isAuth, newProduct);
//edit product
app.put("/products/:productId", isAuth, editProduct);
//delete product
app.delete("/products/:productId", isAuth, deleteProduct);
>>>>>>> final commit
//rate product
app.post("/products/:productId/rate", isAuth, rateProduct);
//edit rating
app.put("/products/:productId/rate", isAuth, editRatingProduct);

//SHOPS
<<<<<<< HEAD
=======
//list shops
app.get("/shops", listShops);
>>>>>>> final commit
//see shop info
app.get("/shops/:shopId", getShop);
//new shop
app.post("/shops", isAuth, createShop);
//edit shop info
<<<<<<< HEAD
app.put("/shops/:shopId", isAuth, isVendor, editShop);
//promote shop
app.put("/shops/:shopId/promote", isAuth, isAdmin, promoteShop);
//unpromote shop
app.put("/shops/:shopId/unpromote", isAuth, isAdmin,unpromoteShop);
//delete shop
app.delete("/shops/:shopId", isAuth, isVendor, deleteShop);
=======
app.put("/shops/:shopId", isAuth, editShop);
//promote shop
app.put("/shops/:shopId/promote", isAuth, isAdmin, promoteShop);
//unpromote shop
app.put("/shops/:shopId/unpromote", isAuth, isAdmin, unpromoteShop);
//delete shop
app.delete("/shops/:shopId", isAuth, deleteShop);
>>>>>>> final commit

//WISHLIST
//list wishlist
app.get("/wishlist/:userId", listWishlist);
//add to wishilist
app.post("/wishlist/:productId", isAuth, addToWishlist);
//delete from wishlist
app.delete("/wishlist/:productId", isAuth, deleteFromWishlist);

//ORDERS
//see cart
app.get("/orders/cart", isAuth, showCurrentCart);
//checkout
app.put("/orders/cart/checkout", isAuth, checkOut);
//finish cart
app.put("/orders/cart/finish", isAuth, finishedCart);
//add to cart
app.post("/orders/cart/:productId", isAuth, addToCart);
//delete from cart
app.delete("/orders/cart/:productId", isAuth, deleteFromCart);
//showfinished orders
app.get("/orders", isAuth, showFinishedOrders);

app.use((error, req, res, next) => {
  console.log(error);
  res.status(error.httpCode || 500).send({
    status: "error",
    message: error.message,
  });
});

app.use((req, res) => {
  res.status(404).send({
    status: "error",
    message: "Not found",
  });
});

app.listen(process.env.PORT, () => {
  console.log("Server chachito");
});
