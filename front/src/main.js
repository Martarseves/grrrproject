import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vueHeadful from "vue-headful";
import dotenv from "dotenv";
import { HeartRating } from "vue-rate-it";

dotenv.config();

Vue.component("vue-headful", vueHeadful);
Vue.component("heart-rating", HeartRating);


import axios from "axios";

import { isLogged, getAuthToken } from "./api/utils";

if (isLogged()) {
  axios.defaults.headers.common["Authorization"] = `Bearer ${getAuthToken()}`;
}


Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
