import Vue from "vue";
import VueRouter from "vue-router";
import { isLogged } from "../api/utils";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("../views/Home.vue"),
    meta: {
      allowAnonymous: true,
    },
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/Login.vue"),
    meta: {
      allowAnonymous: true,
    },
    beforeEnter: (to, from, next) => {
      if (isLogged()) {
        next({
          path: "/",
          query: { redirect: to.fullPath },
        });
      } else {
        next();
      }
    },
  },
  {
    path: "/register",
    name: "Register",
    component: () => import("../views/Register.vue"),
    meta: {
      allowAnonymous: true,
    },
    beforeEnter: (to, from, next) => {
      if (isLogged()) {
        next({
          path: "/",
          query: { redirect: to.fullPath },
        });
      } else {
        next();
      }
    },
  },
  {
    path: "/validate",
    name: "Validation",
    component: () => import("../views/Validation.vue"),
    meta: {
      allowAnonymous: true,
    },
  },
  {
    path: "/search",
    name: "Search",
    component: () => import("../views/Search.vue"),
    meta: {
      allowAnonymous: true,
    },
  },
  {
    path: "/product/:id",
    name: "Product",
    component: () => import("../views/Product.vue"),
    meta: {
      allowAnonymous: true,
    },
  },
  {
    path: "/shop/:id",
    name: "Shop",
    component: () => import("../views/Shop.vue"),
    meta: {
      allowAnonymous: true,
    },
  },
  {
    path: "/wishlist/:userId",
    name: "Wishlist",
    component: () => import("../views/Wishlist.vue"),
    meta: {
      allowAnonymous: true,
    },
  },
  {
    path: "/orders",
    name: "Orders",
    component: () => import("../views/Orders.vue"),
    meta: {
      allowAnonymous: false,
    },
  },
  {
    path: "/addresses",
    name: "Addresses",
    component: () => import("../views/Addresses.vue"),
    meta: {
      allowAnonymous: false,
    },
  },
  {
<<<<<<< HEAD
    path: "/profile",
    name: "Profile",
    component: () => import("../views/Profile.vue"),
=======
    path: "/account",
    name: "Account",
    component: () => import("../views/Account.vue"),
>>>>>>> final commit
    meta: {
      allowAnonymous: false,
    },
  },
  {
    path: "/cart",
    name: "Cart",
    component: () => import("../views/Cart.vue"),
    meta: {
      allowAnonymous: false,
    },
  },
  {
    path: "/checkout/:addressId",
    name: "Checkout",
    component: () => import("../views/Checkout.vue"),
    meta: {
      allowAnonymous: false,
    },
  },
  {
    path: "/about",
    name: "About",
    component: () => import("../views/About.vue"),
    meta: {
      allowAnonymous: true,
    },
  },
  {
    path: "/blog",
    name: "Blog",
    component: () => import("../views/Blog.vue"),
    meta: {
      allowAnonymous: true,
    },
  },
  {
<<<<<<< HEAD
=======
    path: "/addshop",
    name: "AddShop",
    component: () => import("../views/AddShop.vue"),
    meta: {
      allowAnonymous: false,
    },
  },
  {
    path: "/addproduct",
    name: "AddProduct",
    component: () => import("../views/AddProduct.vue"),
    meta: {
      allowAnonymous: false,
    },
  },
  {
>>>>>>> final commit
    path: "*",
    name: "Error",
    component: () => import("../views/Error.vue"),
    meta: {
      allowAnonymous: true,
    },
  },
];

const router = new VueRouter({
  routes,
});


router.beforeEach((to, from, next) => {
  if (!to.meta.allowAnonymous && !isLogged()) {
    next({
      path: "/login",
      query: { redirect: to.fullPath },
    });
  } else {
    next();
  }
});

export default router;
