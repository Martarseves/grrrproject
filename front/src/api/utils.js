import axios from "axios";
import jwt from "jwt-decode";

const ENDPOINT = process.env.VUE_APP_API_URL;
const AUTH_TOKEN_KEY = "authToken";
const ROLE = "role";
const USERID = "userId";





export function loginUser(email, password) {
  return new Promise(async (resolve, reject) => {
    try {
      let res = await axios({
        url: `${ENDPOINT}/users/login`, 
        method: "POST", 
        data: {
          email: email,
          password: password, 
        }, 
      });
      setAuthToken(res.data.data.token);

      const { userId, role } = jwt(res.data.data.token);

      setUserId(userId);

      setRole(role);

      resolve();
    } catch (error) {
      reject(error);
    }
  });
}




//AUTH


export function setAuthToken(token) {
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  localStorage.setItem(AUTH_TOKEN_KEY, token);
}

export function clearLogin() {
  axios.defaults.headers.common["Authorization"] = "";
  localStorage.removeItem(AUTH_TOKEN_KEY);
  clearRole();
  clearUserId();
}

export function getAuthToken() {
  return localStorage.getItem(AUTH_TOKEN_KEY);
}

export function getTokenExpirationDate(encodedToken) {
  let token = jwt(encodedToken);
  if (!token.exp) {
    return null;
  }

  let date = new Date(0);
  date.setUTCSeconds(token.exp);
  return date;
}

export function isTokenExpired(token) {
  let expirationDate = getTokenExpirationDate(token);
  return expirationDate < new Date();
}

export function isLogged() {
  let authToken = getAuthToken();

  return !!authToken && !isTokenExpired(authToken);
}




//ROLE


export function setRole(role) {
  localStorage.setItem(ROLE, role);
}

export function clearRole() {
  return localStorage.removeItem(ROLE);
}

export function getRole() {
  return localStorage.getItem(ROLE);
}

export function checkAdmin() {
  let role = getRole();
  let isAdmin = false;

  if (role === "admin") {
    isAdmin = true;
  } else {
    isAdmin = false;
  }

  return isAdmin;
}




//CHECK USER ID


export function setUserId(id) {
  localStorage.setItem(USERID, id);
}

export function clearUserId() {
  return localStorage.removeItem(USERID);
}

export function getUserId() {
  return localStorage.getItem(USERID);
}
