CREATE USER martars IDENTIFIED BY '123456';
CREATE DATABASE  IF NOT EXISTS `grrrDB`;
GRANT ALL PRIVILEGES ON grrrDB.* TO martars;
USE `grrrDB`;


DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `orders_products`;
DROP TABLE IF EXISTS `addresses`;
DROP TABLE IF EXISTS `shops`;
DROP TABLE IF EXISTS `products`;
DROP TABLE IF EXISTS `orders`;
DROP TABLE IF EXISTS `ratings`; 
DROP TABLE IF EXISTS `wishlists`;


CREATE TABLE `users` (
  `id` INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(50) NOT NULL,
  `photo` VARCHAR(255) DEFAULT NULL,
  `first_name` VARCHAR(50) DEFAULT NULL,
  `last_name` VARCHAR(50) DEFAULT NULL,
  `password` VARCHAR(255) DEFAULT NULL,
  `forced_expiration_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` ENUM('regular','vendor','admin') NOT NULL DEFAULT 'regular',
  `creation_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `active` BOOLEAN NOT NULL DEFAULT '0',
  `verification_code` VARCHAR(255) DEFAULT NULL,
  UNIQUE KEY `email` (`email`)
);


CREATE TABLE `addresses` (
  `id` INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `users_id` INT(10) UNSIGNED DEFAULT NULL,
  `alias` VARCHAR(255) DEFAULT NULL,
  `name` VARCHAR(255) DEFAULT NULL,
  `street` VARCHAR(255) DEFAULT NULL,
  `city` VARCHAR(255) DEFAULT NULL,
  `council` VARCHAR(100) DEFAULT NULL,
  `creation_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `FK_addresses_users` (`users_id`),
  CONSTRAINT `FK_addresses_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
);


CREATE TABLE `shops` (
  `id` INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `users_id` INT UNSIGNED DEFAULT NULL,
  `name` VARCHAR(100) DEFAULT NULL,
  `description` TEXT,
  `instagram` VARCHAR(255) DEFAULT NULL,
  `promoted` BOOLEAN DEFAULT '0',
  `creation_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `users_id` (`users_id`),
  CONSTRAINT `FK_shops_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
); 


CREATE TABLE `products` (
  `id` INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `shops_id` INT UNSIGNED DEFAULT NULL,
  `category` VARCHAR(50) DEFAULT NULL,
  `photo` BLOB DEFAULT NULL,
  `name` VARCHAR(100) DEFAULT NULL,
  `price` DECIMAL(7,2) DEFAULT NULL,
  `stock` SMALLINT(6) DEFAULT NULL,
  `available` BOOLEAN DEFAULT '0',
  `type` ENUM('ready','custom') NOT NULL DEFAULT 'ready',
  `description` TEXT,
  `creation_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `FK_products_shops` (`shops_id`),
  CONSTRAINT `FK_products_shops` FOREIGN KEY (`shops_id`) REFERENCES `shops` (`id`)
);


CREATE TABLE `orders` (
  `id` INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `users_id` INT UNSIGNED DEFAULT NULL,
  `addresses_id` INT UNSIGNED DEFAULT NULL,
  `finished` BOOLEAN DEFAULT '0',
  `sell_date` DATETIME DEFAULT NULL,
  `creation_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `FK_orders_addresses` (`addresses_id`),
  KEY `FK_orders_users` (`users_id`),
  CONSTRAINT `FK_orders_addresses` FOREIGN KEY (`addresses_id`) REFERENCES `addresses` (`id`) ON DELETE SET NULL,
  CONSTRAINT `FK_orders_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
); 


CREATE TABLE `ratings` (
  `id` INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `users_id` INT UNSIGNED DEFAULT NULL,
  `products_id` INT UNSIGNED DEFAULT NULL,
  `rating` DECIMAL(2,1) DEFAULT NULL,
  `comment` VARCHAR(255) DEFAULT NULL,
  KEY `FK_ratings_users` (`users_id`),
  KEY `FK_rating_products` (`products_id`),
  CONSTRAINT `FK_rating_products` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_ratings_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
); 


CREATE TABLE `wishlists` (
  `id` INT UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `users_id` INT UNSIGNED DEFAULT NULL,
  `products_id` INT UNSIGNED DEFAULT NULL,
  `creation_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mod_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `FK_wishlist_users` (`users_id`),
  KEY `FK_wishlist_products` (`products_id`),
  CONSTRAINT `FK_wishlist_products` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`),
  CONSTRAINT `FK_wishlist_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`)
); 


CREATE TABLE `orders_products` (
  `id` INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  `order_id` INT UNSIGNED DEFAULT NULL,
  `product_id` INT UNSIGNED DEFAULT NULL,
  `price` DECIMAL(7,2) DEFAULT NULL
  `quantity` INT DEFAULT '1',
  CONSTRAINT `FK_orders_products_products` FOREIGN KEY(`product_id`) REFERENCES products(id),
  CONSTRAINT `FK_orders_products_orders` FOREIGN KEY (`order_id`) REFERENCES orders(id)
); 